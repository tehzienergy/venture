window.sr = ScrollReveal();

sr.reveal('.animated', { 
  origin: 'bottom',
  duration: 800,
  easing: 'ease-in-out',
  distance: '50px',
  scale: 1,
  mobile: false,
  reset: false,
  opacity: 0,
//  viewFactor: .2,
});
$('.slider').slick({
  dots: true,
  arrows: true,
  responsive: [
    {
      breakpoint: 1199,
      settings: {
        arrows: false
      }
    },
  ]
})